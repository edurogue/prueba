const express = require("express");
const app = express();

app.get("/", function (req, res) {
  res.send("Hello World");
});

app.listen(80, (e) => {
  console.log(e || "puerto on 80");
});
